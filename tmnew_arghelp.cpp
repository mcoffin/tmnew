#include <array>
#include <string_view>
#include <string>
#include <cstdio>
#include <getopt.h>
#include "tmnew_arghelp.hpp"
#include "tmnew_utils.hpp"

std::ostream& tmnew::operator<<(std::ostream& s, const ArgumentHelp& v) {
	s << '-' << v.arg;
	if (v.help_message.has_value()) {
		s << " - " << v.help_message.value_or("");
	}
	return s;
}

template<std::size_t N>
class message_array : public std::array<char, N> {
public:
	inline explicit message_array(const char *fmt, ...)
	{
		va_list args;
		va_start(args, fmt);
		len = std::vsnprintf(this->data(), N, fmt, args);
		va_end(args);
	}
	inline constexpr operator std::string_view () const& {
		return std::string_view(this->data(), len);
	}
private:
	size_t len;
};

class unknown_argument_message : public std::array<char, 21> {
public:
	constexpr explicit unknown_argument_message(const char o):
		std::array<char, 21>{'U','n','k','n','o','w','n',' ','A','r','g','u','m','e','n','t',':',' ','-',o,'\0'}
	{}
	inline operator std::string_view () const& {
		return std::string_view(data(), size() - 1);
	}
};
tmnew::UnknownArgumentError::UnknownArgumentError(char o):
	std::runtime_error(unknown_argument_message(o).data()),
	opt(o)
{}

tmnew::RequiredArgumentError::RequiredArgumentError(const char o):
	std::runtime_error(message_array<128>("Invalid argument: %c requires an argument", o).data()),
	opt(o)
{}
