.DEFAULT_GOAL := all

CC ?= clang
CXX ?= clang++
CFLAGS += -std=gnu11
CXXFLAGS += -std=c++20
EXECUTABLE_NAME ?= tmnew
PREFIX ?= $(HOME)/.local

ifneq ($(USE_NATIVE),)
	EXTRA_CFLAGS += -march=native
endif

ifneq ($(USE_LTO),)
	EXTRA_CFLAGS += -flto
	LDFLAGS += -flto
endif

ifeq ($(DEBUG),)
	EXTRA_CFLAGS += -O3 -fno-plt -DNDEBUG
else
	EXTRA_CFLAGS += -g
endif
CFLAGS += $(EXTRA_CFLAGS)
CXXFLAGS += $(EXTRA_CFLAGS)
CXXFLAGS += -fvisibility=hidden
ifneq ($(CXX_STDLIB),)
	CXXFLAGS += -stdlib=$(CXX_STDLIB)
endif

LDFLAGS += -flto
ifeq ($(CC),clang)
	LDFLAGS += -fuse-ld=lld
else ifeq ($(CXX),clang++)
	LDFLAGS += -fuse-ld=lld
endif

c_sources := $(shell find . -mindepth 1 -maxdepth 1 -type f -name '*.c')
cxx_sources := $(shell find . -mindepth 1 -maxdepth 1 -type f -name '*.cpp')
c_headers := $(shell find . -mindepth 1 -maxdepth 1 -type f -name '*.h')
cxx_headers := $(shell find . -mindepth 1 -maxdepth 1 -type f -name '*.hpp')
c_objects := $(c_sources:.c=.o)
cxx_objects := $(cxx_sources:.cpp=.o)
all_headers := $(c_headers)
all_headers += $(cxx_headers)
all_objects := $(c_objects) $(cxx_objects)

ifneq ($(cxx_sources),)
	LDCC := $(CXX)
	LDCFLAGS := $(CXXFLAGS)
else
	LDCC := $(CC)
	LDCFLAGS := $(CFLAGS)
endif

%.o: %.c $(c_headers)
	$(CC) $(CFLAGS) -c $< -o $@

%.o: %.cpp $(all_headers)
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(EXECUTABLE_NAME): $(all_objects)
	$(LDCC) $(LDCFLAGS) $(LDFLAGS) $^ -o $@

debug:
	@printf '\033[1;36m%s\033[0m=%s\n' CC "$(CC)"
	@printf '\033[1;36m%s\033[0m=%s\n' CXX "$(CXX)"
	@printf '\033[1;36m%s\033[0m=%s\n' CFLAGS "$(CFLAGS)"
	@printf '\033[1;36m%s\033[0m=%s\n' CXXFLAGS "$(CXXFLAGS)"
	@printf '\033[1;36m%s\033[0m=%s\n' LDFLAGS "$(LDFLAGS)"

clean:
	[ ! -e $(EXECUTABLE_NAME) ] || rm $(EXECUTABLE_NAME)
	$(foreach f,$(all_objects),[ ! -e "$(f)" ] || rm "$(f)";)

all: $(EXECUTABLE_NAME)

install: $(EXECUTABLE_NAME)
	install -Dm 755 -t $(PREFIX)/bin ./$(EXECUTABLE_NAME)

.PHONY: clean debug all
