#include <array>
#include <iostream>
#include <string>
#include <string_view>
#include <optional>
#include <stdexcept>
#include <system_error>
#include <type_traits>

#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <errno.h>
#include <getopt.h>

#include "tmnew_arghelp.hpp"
#include "tmnew_utils.hpp"

namespace tmnew {
	using namespace tmnew::utils;
	using namespace std::literals::string_view_literals;

	static const std::string tmux_executable = "tmux";

	static void print_usage(std::ostream& s, std::optional<std::string_view> invocation = std::nullopt);

	class Args {
	public:
		explicit Args(int argc, char **argv):
			status(std::nullopt),
			attach(false)
		{
			const std::string_view invocation(argv[0]);
			int opt;
			auto _print_usage = [&, invocation](std::ostream& s) {
				return print_usage(s, invocation);
			};
			while ((opt = getopt(argc, argv, ":han:")) != -1) {
				switch (opt) {
					case 'a':
						attach = true;
						break;
					case 'n':
						if (!set_session_name(std::string_view(optarg))) {
							throw RequiredArgumentError(optopt);
						}
						break;
					case 'h':
						print_usage(std::cout);
						set_exit(EXIT_SUCCESS);
						break;
					case ':':
						throw RequiredArgumentError();
						break;
					case '?':
					default:
						throw UnknownArgumentError(optopt);
						break;
				}
			}
		}
		inline bool should_exit() const& noexcept {
			return status.has_value();
		}
		std::string get_session_name() const& {
			if (session_name.has_value()) {
				return session_name.value();
			} else {
				try {
					return std::string(utils::get_login_name());
				} catch (const std::runtime_error& e) {
					std::cerr << "warning: Failed to get username: " << e.what() << std::endl;
					return "unknown";
				}
			}
		}
		int run_main() const& {
			// Unset DRI_PRIME in case the terminal was run with it
			utils::_unsetenv("DRI_PRIME");
			// Set up tmux and run it
			// std::string name = session_name.has_value() ? session_name.value() : std::string(get_login_name());
			auto name = get_session_name();

			if (attach) {
				const std::array<const char*, 5> tmux_args {"-u", "a", "-t", name.c_str(), NULL};
				return utils::_execvp(tmux_executable, tmux_args);
			} else {
				const std::array<const char*, 5> tmux_args {"-u", "new", "-s", name.c_str(), NULL};
				return utils::_execvp(tmux_executable, tmux_args);
			}
		}
		inline int get_status() const& noexcept {
			return status.value_or(EXIT_SUCCESS);
		}
		std::optional<int> status;
		bool attach;
	protected:
		std::optional<std::string> session_name;
	private:
		inline void set_exit(int _status) & {
			status = _status;
		}
		inline bool set_session_name(std::string_view name) & {
			if (name.length() > 0) {
				session_name = name;
				return true;
			} else {
				return false;
			}
		}
	};

	static const std::array<ArgumentHelp, 3> help_args {
		ArgumentHelp('a', "attach, instead of creating a new session"),
		ArgumentHelp('s', "override session name (defaults to login username)"),
		ArgumentHelp('h', "print this help message, then exit")
	};

	static const std::array<std::string, 1> authors {
		"Matt Coffin <mcoffin13@gmail.com>"
	};

	template<typename Sep, typename It>
	static void print_separated(std::ostream& s, Sep sep, It values, const It end) {
		if (values != end) {
			s << *values;
			for (values = values + 1; values != end; values++) {
				s << sep << *values;
			}
		}
		// if (values.size() < 1) {
		// 	return;
		// }
		// s << values[0];
		// for (auto v = (values.begin() + 1); v != values.end(); v++) {
		// 	s << sep << v;
		// }
	}

	static void print_usage(std::ostream& s, std::optional<std::string_view> invocation) {
		using std::endl;
		const auto invoc = invocation.value_or("tmnew"sv);
		s << "Usage: " << invoc << " [-a] [-s SESSION_NAME] [-h]" << endl;
		s << "Author: ";
		print_separated(s, ", "sv, authors.cbegin(), authors.cend());
		s << endl << endl << "Options:" << endl;

		for (const ArgumentHelp& a : help_args) {
			s << '\t' << a << endl;
		}
	}
}

static inline int error_exit_code(const std::runtime_error& e) {
	auto se = dynamic_cast<const std::system_error*>(&e);
	const int ret = se != nullptr ? se->code().value() : EXIT_FAILURE;
	return ret != 0 ? ret : EXIT_FAILURE;
}

__attribute__ ((visibility ("default")))
int main(int argc, char *argv[]) {
	using namespace tmnew;

	auto invocation = argv[0];
	auto print_err = [invocation](std::string_view e) {
		std::cerr << invocation << ": " << e << std::endl;
	};

	try {
		const Args args(argc, argv);

		// If we encountered an exit condition during argument parsing
		// then just exit
		if (args.should_exit()) {
			return args.get_status();
		}

		return args.run_main();
	} catch (std::runtime_error& e) {
		print_err(e.what());
		return error_exit_code(e);
	}
}
