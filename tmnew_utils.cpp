#include "tmnew_utils.hpp"

#include <string_view>
#include <system_error>
#include <type_traits>

#include <errno.h>
#include <unistd.h>

std::string_view tmnew::utils::get_login_name() {
	auto s = getlogin();
	if (s == nullptr) {
		throw std::system_error(errno, std::generic_category());
	}
	return s;
}

template<class F>
static inline void wrapped_system_error_errno(F f) {
	static_assert(std::is_invocable<F>());
	const auto status = f();
	if (status != 0) {
		throw std::system_error(errno, std::generic_category());
	}
}

void tmnew::utils::_unsetenv(const std::string& k) {
	wrapped_system_error_errno([k]() -> int {
		return unsetenv(k.c_str());
	});
}
