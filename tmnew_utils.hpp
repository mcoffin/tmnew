#pragma once
#include <array>
#include <string>
#include <string_view>
#include <unistd.h>

namespace tmnew::utils {
	std::string_view get_login_name();
	void _unsetenv(const std::string& k);

	template<size_t N>
	inline int _execvp(const std::string& f, const std::array<const char *, N>& args) {
		return execvp(f.c_str(), const_cast<char**>(&args[0]));
	}

	template<size_t N>
	inline std::string print_message(const char* fmt, ...) {
		std::array<char, N> buf;
		va_list args;
		va_start(args, fmt);
		const auto len = std::vsnprintf(buf.data(), buf.size(), fmt, args);
		va_end(args);
		return std::string(std::string_view(buf.data(), len));
	}
}
