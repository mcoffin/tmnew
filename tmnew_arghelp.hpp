#pragma once
#include <array>
#include <ostream>
#include <optional>
#include <string_view>
#include <stdexcept>
#include <cstdio>

namespace tmnew {
	class ArgumentHelp {
	public:
		constexpr explicit ArgumentHelp(char _arg) noexcept :
			arg(_arg)
		{}
		constexpr explicit ArgumentHelp(char _arg, const std::string_view _help_message) noexcept :
			arg(_arg),
			help_message(_help_message)
		{}
		friend std::ostream& operator<<(std::ostream& s, const ArgumentHelp& v);
	private:
		char arg;
		std::optional<std::string_view> help_message;
	};

	class UnknownArgumentError : public std::runtime_error {
	public:
		explicit UnknownArgumentError(char o);
		inline char option() const noexcept {
			return opt;
		}
	private:
		char opt;
	};

	class RequiredArgumentError : public std::runtime_error {
	public:
		explicit RequiredArgumentError(const char o = optopt);
		inline char option() const noexcept {
			return opt;
		}
	private:
		char opt;
	};
}
